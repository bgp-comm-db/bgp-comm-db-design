Types translating Inheritance into RDB
======================================

	+ Single Table Inheritance (STI)
		- "One table per hierarchy", "Eine Tabelle pro Klassenhierarchie"
		-
	+ Multiple Table Inheritance (MTI)
		* Concrete Table Inheritance
			- "One table per concrete class", "Eine Tabelle pro konkreter Klasse"
		* Class Table Inheritance
			- "One table per class", "Eine Tabelle pro Klasse"