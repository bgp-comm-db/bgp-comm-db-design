as.name
-------

longest in bgp.potaroo.net/cidr/autnums.html
N24-GESELLSCHAFT-FUER-NACHRICHTEN-UND-ZEITGESCHEHEN-MBH-AS
58 chars

no length specified in:
 * http://tools.ietf.org/html/rfc2622
 * http://tools.ietf.org/html/rfc2650
 * http://tools.ietf.org/html/rfc4012

=> 70 chars

peertype.type
-------------

estimated ~ one word
=> 30 char

ixp.name
--------

estimated ~ one word
=> 30 char

ext_comm_type.name
-----------------

Longest in http://www.iana.org/assignments/bgp-extended-communities/bgp-extended-communities.xhtml
Non-Transitive Four-Octet AS-Specific Extended Community (Sub-Types are defined in the "Non-Transitive Four-octet AS-Specific Extended Community Sub-Types" registry)

estimated
=> 200